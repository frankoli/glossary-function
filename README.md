# Glossary Function

This project is part of a bachelor thesis focusing on software development for
knowledge preservation - Glossary. This part of the thesis focuses on creating a function that uses the Glossary.
The Glossary itself is at: https://gitlab.fit.cvut.cz/frankoli/glossary

> **Limitations:** <br>
> - Function is not available for iOS devices and Safari browser <br>
> - Function is available for browsers: Chrome, Firefox, Edge <br>
> - In this moment function marked only one world terms.

Function may be supported also for browsers listed [here](https://caniuse.com/js-regexp-lookbehind). 
Function is not available for all browsers because of missing JS implementation for negative lookbehind.


## How to use 

- It is necessary to add to the header following lines:
```html
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" ></script>
```
- It is necessary to add styling for border:
```html
<style>
    .btn-outline-success{
        padding: 2px 4px;
        border-color: transparent;
        padding-top: 0;
    }
    .btn-outline-success:hover {
        background-color: transparent;
        color: #198754;
        border-color: #198754;
        padding-top: 0;
    }
</style>
```

> you can change appearance of of marked terms by changing style of `btn-outline-success` and `btn-outline-success:hover`

- And also is necessary to add path to downloaded function
- Add function to `<script>` tags.


## Parametres
**path_to_glossary** main url to glossary (for example `"http://bc-glossary.herokuapp.com/"`)

**lang** determines in which language the glosses will be searched (for example `"cz"`). You can find out all possible variants by go to 
`<url_to_glossary>/graphql` and type
```graphql
query{
    languages{
        name
    }
}
```
**tags** determines which tags the searched glosses should have (for example `["animal", "glossary"]`). You can find out all possible variants by go to 
`<url_to_glossary>/graphql` and type
```graphql
query{
  tags{
    name
  }
}
```

