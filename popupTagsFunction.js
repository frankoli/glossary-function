async function createPopUps(path_to_glossary="http://127.0.0.1:8000/",
                            lang="cz",
                            tags=[]) {
    // const path_to_glossary = path

    async function getDataGraphQL(lang, text, tags) {
        let q_language = "language:\"" + lang.replaceAll('\n', '') + "\"";
        if (lang.replaceAll('\n', '') === ""){
            q_language = ""
        }
        let q_tags = "";
        if (tags.length !== 0){
            q_tags = "tags:["
            for ( let i = 0 ; i < tags.length; i++) {
                q_tags += "\"" + tags[i].replaceAll('\n', '') + "\", "
            }
            q_tags += "]"
        }
        const query = `
            {
              findIn(text: "` + text.replaceAll('\n', '') + `",`+q_language + q_tags +`
                 
              ){
                word,
                definition{
                  language{
                    name
                  }
                  concept{
                    id
                  }
                  abstract,
                  file
                }
              }
            }
        `
        const res = await fetch(path_to_glossary + "graphql", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({query: query})
        })
        return res.json()


    }


    async function myFunction() {
        const paragraphs = document.getElementsByTagName("p");
        let popupId = 0
        for (let i = 0; i < document.getElementsByTagName("p").length; i++) {
            let size = getComputedStyle(paragraphs[i]).fontSize
            let family = getComputedStyle(paragraphs[i]).fontFamily
            let style_popup = "style=\"font-size: " + size + "; font-family:" + family + "\"";
            let text = paragraphs[i].innerHTML.replaceAll("'", "").replaceAll('"', '')
            let text_query = await getDataGraphQL(lang, text, tags);
            let result = text_query.data;
            if (result !== undefined) {
                let gloss = result.findIn
                for (let index = 0; index < gloss.length; index++) {
                    const concept = gloss[index].definition[0].concept.id
                    let abstract = gloss[index].definition[0].abstract
                    let file = gloss[index].definition[0].file
                    if (abstract === "") {
                        abstract = file.substring(0, 255)
                    }
                    const href = path_to_glossary + "gloss/"
                        + concept
                        + "/?lang=" + gloss[index].definition[0].language.name.toLowerCase()
                        + "&searched="

                    const popupTitle = "<a href='" + href + "'>" + gloss[index].word + "</a>"
                    const dostHref = "<a href='" + href + "' title='" + gloss[index].word + "'>...</a>"
                    abstract = abstract + dostHref
                    abstract = abstract.replaceAll('\n', '')


                    const replace = "<button " + style_popup + " type=\"button\" " +
                        "class=\"btn btn-outline-success" +
                        "\"data-bs-toggle=\"popover\"" +
                        "data-bs-content=\"" + abstract + "\">" +
                        gloss[index].word + "</button>"
                    var regEx = new RegExp("(?<!<[^>]*)" + gloss[index].word, "gi");
                    text = text.toString().replace(regEx, replace)
                    popupId++;
                }
                paragraphs[i].innerHTML = text
            }

        }
    }

    window.addEventListener("load", async function () {
        await myFunction();
        $(document).ready(function () {
            $('[data-bs-toggle="popover"]').popover({html: true, trigger: "manual", animation: false})
              .on("mouseenter", function() {
                var _this = this;
                $(this).popover("show");
                $(".popover").on("mouseleave", function() {
                  $(_this).popover('hide');
                });
              }).on("mouseleave", function() {
                var _this = this;
                setTimeout(function() {
                  if (!$(".popover:hover").length) {
                    $(_this).popover("hide");
                  }
                }, 300);
              });
        });
    });

}